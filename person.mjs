export default class Person {
    constructor(name) {
        this.name = name;
    }

    say(phrase) {
        return `${this.name} says ${phrase}`;
    }
    shakes_hands(person){
        return `${this.name} shakes_hands ${person.name}`;
    }
}

